all: $(patsubst %.dot, public/%.pdf, $(wildcard *.dot))

public/%.pdf: %.dot public/
	$(DOT) -Tpdf $< -o $@

public/:
	mkdir -p public

DOT ?=dot
